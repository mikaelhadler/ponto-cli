module.exports = {
  generateAuthQuestions() {
    return [
      {
        type: 'text',
        name: 'user',
        message: 'Username',
      },
      {
        type: 'password',
        name: 'pass',
        message: 'Password',
      },
    ];
  },
  generateAppointmentsForm() {
    return [
      {
        type: 'text',
        name: 'date',
        message: 'Date'
      },
      {
        type: 'text',
        name: 'entryOne',
        message: 'Start (start day)',
      },
      {
        type: 'text',
        name: 'outputOne',
        message: 'Output (output lunch)',
      },
      {
        type: 'text',
        name: 'entryTwo',
        message: 'End (entry lunch)',
      },
      {
        type: 'text',
        name: 'outputTwo',
        message: 'End (Finish day)',
      },
      {
        type: 'text',
        name: 'reason',
        message: 'Reason to appointments'
      }
    ]
  },  
  generateFormQuestions({ projects, activities }) {
    return [
      {
        type: 'text',
        name: 'date',
        message: 'Date',
      },
      {
        type: 'text',
        name: 'startBefore',
        message: 'Start (before lunch)',
      },
      {
        type: 'text',
        name: 'endBefore',
        message: 'End (before lunch)',
      },
      {
        type: 'text',
        name: 'startAfter',
        message: 'Start (after lunch)',
      },
      {
        type: 'text',
        name: 'endAfter',
        message: 'End (after lunch)',
      },
      {
        type: 'autocomplete',
        name: 'project',
        message: 'Project',
        choices: projects,
      },
      {
        type: 'autocomplete',
        name: 'activity',
        message: 'Activity',
        choices: activities,
      },
      {
        type: 'text',
        name: 'description',
        message: 'Description',
      },
    ];
  },
  generateRepeatQuestion() {
    return [
      {
        type: 'confirm',
        name: 'repeat',
        message: 'Repeat?',
        initial: false,
      },
    ];
  },
};
