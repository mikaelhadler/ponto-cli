module.exports = {
  BASE_URL: 'https://pontosecullum4-01.secullum.com.br/ponto4web/821220936#login',
  HOURS_PER_DAY: 9,
  PUPPETEER_CONFIG: {
    slowMo: 500,
  },
};