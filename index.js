const Table = require('cli-table');
const prompts = require('prompts');
const puppeteer = require('puppeteer');

const { getWeekdays } = require('./utils');
const { generateAuthQuestions, generateAppointmentsForm, generateRepeatQuestion } = require('./questions');
const { BASE_URL, PUPPETEER_CONFIG, HOURS_PER_DAY } = require('./config');

const style = {
  head: ['yellow'],
  border: ['white'],
  compact: false,
};

const activitiesTable = new Table({
  head: ['Date', 'Start', 'End', 'Project', 'Activity', 'Total'],
  colAligns: ['left', 'middle', 'middle', 'middle', 'middle', 'right'],
  style,
});

activitiesTable.indexes = [0, 1, 2, 3, 7, 8];

const hoursTable = new Table({
  head: ['Total Month Hours', 'Current Hours', 'Missing Hours'],
  colAligns: ['left', 'middle', 'right'],
  style,
});

(async () => {
  const credentials = await prompts(generateAuthQuestions());
  const appointments = await prompts(generateAppointmentsForm());
  const repeat = await prompts(generateRepeatQuestion());

  const browser = await puppeteer.launch(PUPPETEER_CONFIG);
  const page = await browser.newPage();

  await page.goto(BASE_URL);

  await page.evaluate(({ user, pass }) => {
    document.querySelector('[name="usuario"]').value = user;
    document.querySelector('[name="senha"]').value = pass;

    document.querySelector('button#login').click();
  }, credentials);

  await page.waitFor('a[href="#alterar-batidas"');

  await page.evaluate(() => {
    document.querySelector('a[href="#alterar-batidas"').click()
  })
  await page.waitFor('input#data');

  do {
    await page.evaluate(({
      date, entryOne, outputOne, entryTwo, outputTwo, reason,
    }) => {
      document.querySelector('[id="data"]').value = date;
      document.querySelector('[id="entrada1"]').value = entryOne;
      document.querySelector('[id="obsEntrada1"]').value = reason;

      document.querySelector('[id="saida1"]').value = outputOne;
      document.querySelector('[id="obsSaida1"]').value = reason;

      document.querySelector('[id="entrada2"]').value = entryTwo;
      document.querySelector('[id="obsEntrada2"]').value = reason;

      document.querySelector('[id="saida2"]').value = outputTwo;
      document.querySelector('[id="obsSaida2"]').value = reason;

      document.querySelector('[id="salvar"]').click();
    }, appointments);

    await page.waitFor(1500);
  } while (repeat === true);

  await browser.close();
})();
