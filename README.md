# Ponto CLI

A CLI to semi-automate globalsys's working hours description fill-out proccess :clock3:

## Requirements

Just Node.js :D

## Getting Started

1. Install all dependencies with `yarn install` or `npm install`.
2. Run with `yarn start` or `npm start`.
