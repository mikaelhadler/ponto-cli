const moment = require('moment');
const business = require('moment-business');

module.exports = {
  getWeekdays() {
    const start = moment().startOf('month');
    const end = moment().endOf('month');
    const weekDays = business.weekDays(start, end);

    return weekDays + 1;
  },
};